use std::cell::Cell;
use std::error::Error;
use std::fmt;
use std::fmt::{Display, Formatter};

#[derive(Debug, PartialEq)]
enum IntCodeError {
    InvalidOp(usize),
    UnexpectedEnd,
    IndexOutOfBounds(usize),
}

impl Display for IntCodeError {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        use IntCodeError::*;
        match self {
            InvalidOp(ip) => f.write_fmt(format_args!("invalid op at {}", *ip)),
            UnexpectedEnd => f.write_str("code ended unexpectedly"),
            IndexOutOfBounds(idx) => f.write_fmt(format_args!("index out of bounds: {}", *idx)),
        }
    }
}

impl Error for IntCodeError {}

#[derive(Debug, PartialEq, Clone)]
struct IntCode {
    mem: Vec<u32>,
    ip: usize,
}

impl IntCode {
    fn new() -> IntCode {
        Vec::new().into()
    }

    fn clone_from(&mut self, other: &IntCode) {
        self.mem.clone_from(&other.mem);
        self.ip = other.ip
    }

    fn input(&mut self, noun: u32, verb: u32) -> Result<(), IntCodeError> {
        *self.get_mut(1)? = noun;
        *self.get_mut(2)? = verb;
        Ok(())
    }

    fn output(&self) -> Result<u32, IntCodeError> {
        self.get(0)
    }

    fn take(&mut self) -> Result<u32, IntCodeError> {
        match self.mem.get(self.ip) {
            Some(code) => {
                self.ip += 1;
                Ok(*code)
            }
            None => Err(IntCodeError::UnexpectedEnd),
        }
    }

    fn get(&self, idx: usize) -> Result<u32, IntCodeError> {
        self.mem
            .get(idx)
            .cloned()
            .ok_or_else(|| IntCodeError::IndexOutOfBounds(idx))
    }

    fn get_mut(&mut self, idx: usize) -> Result<&mut u32, IntCodeError> {
        self.mem
            .get_mut(idx)
            .ok_or_else(|| IntCodeError::IndexOutOfBounds(idx))
    }

    fn step(&mut self) -> Result<Option<()>, IntCodeError> {
        use IntCodeError::*;
        let op = self.take()?;

        if op == 99 {
            return Ok(None);
        }

        if op != 1 && op != 2 {
            return Err(InvalidOp(self.ip - 1));
        }

        let idx_a = self.take()? as usize;
        let idx_b = self.take()? as usize;
        let idx_out = self.take()? as usize;

        let val_a = self.get(idx_a)?;
        let val_b = self.get(idx_b)?;

        let result = match op {
            1 => val_a + val_b,
            2 => val_a * val_b,
            _ => unreachable!(),
        };

        *self.get_mut(idx_out)? = result;

        Ok(Some(()))
    }

    fn run(&mut self) -> Result<(), IntCodeError> {
        while let Some(_) = self.step()? {
            //noop
        }
        Ok(())
    }
}

impl Display for IntCode {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        let string = self
            .mem
            .iter()
            .map(|i| i.to_string())
            .collect::<Vec<_>>()
            .join(",");
        f.write_str(&string)
    }
}

impl From<Vec<u32>> for IntCode {
    fn from(mem: Vec<u32>) -> Self {
        IntCode { mem, ip: 0 }
    }
}

#[aoc_generator(day2)]
fn input_generator(input: &str) -> Cell<IntCode> {
    let mem: Vec<u32> = input.split(",").map(|code| code.parse().unwrap()).collect();
    IntCode::from(mem).into()
}

#[aoc(day2, part1)]
fn solve_part1(code: &Cell<IntCode>) -> Result<u32, IntCodeError> {
    let mut code = code.replace(IntCode::new());

    code.input(12, 2)?;
    code.run()?;
    code.get(0)
}

#[aoc(day2, part2)]
fn solve_part2(code: &Cell<IntCode>) -> Result<u32, IntCodeError> {
    let template = code.replace(IntCode::new());

    let mut code = template.clone();

    for noun in 0..=99u32 {
        for verb in 0..=99u32 {
            code.input(noun, verb)?;
            code.run()?;
            if code.output()? == 19690720 {
                return Ok(100 * noun + verb);
            }
            code.clone_from(&template);
        }
    }

    unreachable!()
}

#[cfg(test)]
mod tests {
    use super::*;

    fn do_part1(input: &str) -> Result<String, IntCodeError> {
        solve_part1(&input_generator(input)).map(|ic| ic.to_string())
    }

    #[test]
    fn test_part1() {
        assert_eq!(do_part1("1,0,0,0,99"), Ok("2,0,0,0,99".to_owned()));
        assert_eq!(do_part1("2,3,0,3,99"), Ok("2,3,0,6,99".to_owned()));
        assert_eq!(do_part1("2,4,4,5,99,0"), Ok("2,4,4,5,99,9801".to_owned()));
        assert_eq!(
            do_part1("1,1,1,4,99,5,6,0,99"),
            Ok("30,1,1,4,2,5,6,0,99".to_owned())
        );
    }
}
