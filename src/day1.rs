#[aoc_generator(day1)]
fn input_generator(input: &str) -> Vec<u32> {
    input.lines().map(|l| l.parse().unwrap()).collect()
}

fn fuel_required(mass: u32) -> u32 {
    (mass / 3).saturating_sub(2)
}

#[aoc(day1, part1)]
fn solve_part1(mass: &[u32]) -> u32 {
    mass.iter().cloned().map(fuel_required).sum()
}

#[aoc(day1, part2)]
fn solve_part2(mass: &[u32]) -> u32 {
    mass.iter()
        .cloned()
        .map(|mut mass| {
            let mut fuel = 0;

            while mass > 0 {
                let new_fuel = fuel_required(mass);
                fuel += new_fuel;
                mass = new_fuel;
            }

            fuel
        })
        .sum()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        assert_eq!(solve_part1(&[12]), 2);
        assert_eq!(solve_part1(&[14]), 2);
        assert_eq!(solve_part1(&[1969]), 654);
        assert_eq!(solve_part1(&[100756]), 33583);
    }

    #[test]
    fn test_part2() {
        assert_eq!(solve_part2(&[14]), 2);
        assert_eq!(solve_part2(&[1969]), 966);
        assert_eq!(solve_part2(&[100756]), 50346);
    }
}
